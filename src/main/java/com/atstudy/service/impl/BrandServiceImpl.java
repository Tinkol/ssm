package com.atstudy.service.impl;

import com.atstudy.domain.entity.Brand;
import com.atstudy.mapper.BrandMapper;
import com.atstudy.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    private BrandMapper brandMapper;
    @Override
    public List<Brand> FindAll() {
        return brandMapper.selectList(null);
    }
}
