package com.atstudy.service;

import com.atstudy.domain.entity.Brand;

import java.util.List;

public interface BrandService {

    List<Brand> FindAll();
}
