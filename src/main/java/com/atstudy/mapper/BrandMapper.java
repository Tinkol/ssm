package com.atstudy.mapper;

import com.atstudy.domain.entity.Brand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface BrandMapper extends BaseMapper<Brand> {

}
