package com.atstudy.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import java.util.Date;

@Data
@TableName("brand")
public class Brand {
    @TableId
    private String brandId;
    private String brandName;
    private String brandIntroduction;
    private String brandLogourl;
    private Integer sortno;
    private Date createtime;
    private Date updatetime;
}
