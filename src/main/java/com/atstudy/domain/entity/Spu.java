package com.atstudy.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@TableName("spu")
public class Spu {
    @TableId
    private Long spuId;
    private String spuName;
    private String spuTitle;
    private String spuIntroduction;
    private String spuUnit;
    private Byte spuSketchtype;
    private String spuSketch;
    private String spuSpecs;
    private Byte spuSkutype;
    private Byte spuStatus;
    private Date createtime;
    private Date updatetime;
    private String spuBrandId;



}
