package com.atstudy.domain.dto;

import lombok.Data;



@Data
public class brandDto {

    private String brandId;
    private String brandName;
    private String brandIntroduction;
    private Integer sortno;
    private Integer page;
    private Integer pagesconext;


}
