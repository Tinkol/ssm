package com.atstudy.controller;

import com.atstudy.domain.entity.Brand;
import com.atstudy.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class BrandController {

    @Autowired
    private BrandService brandService;
    @GetMapping("/brand")
    public String index(Model model){
        List<Brand> brands = brandService.FindAll();
        model.addAttribute("brandList",brands);
        return "index";
    }
}
